"use strict";
// vim:et:sw=4:ts=4:sts=4

angular.module('angulardf', []).

/** Provides an RDF quad store and fetcher with an anular.js tuned API.
 *
 * As of now, this works as a directive that unconditionally creates an .ardf
 * object on the scope (as would a ng-controller"angualrdf as ardf" were this a
 * controller). Some of the methods are also direcly stored on the scope for
 * compatibility with earlier versions.
 *
 * The ardf object has the following methods:
 *
 * * subjects, predicates, objects, whys (query methods)
 *
 *     The main access functions to the quad store. Passed the other nodes as
 *     parts of a statement (eg. for subject: predicate, object, why), they
 *     return all RDF nodes that satisfy the given constraints. Arguments whose
 *     value is null or which are not given are wildcards.
 *
 *     If any argument is undefined, an empty list is returned. This behavior
 *     is tailored towards AngularJS applications, where arguments might not
 *     have stabilized still have undefined values. Use null or do not specify
 *     an argument at all to create wild cards.
 *
 *     Also in adaption to AngularJS (where we do not expose the $rdf.sym
 *     function), strings are accepted as arguments and (unlike in native
 *     rdflib functions) are interpreted as URIs. This is deprecated in favor
 *     of using namespaces.
 *
 *     All arguments also accept lists (anything that has a .length) as
 *     arguments, and will return the union of all combinations of those query
 *     arguments. This supports constructs like objects(objects(A, x), y) to
 *     get all Z where { A x B . B y Z . }. That invocation neither does the
 *     string conversion magic nor produces empty results if undefined shows up
 *     in the list (in the latter case, it will produce matches from all values
 *     of that position, in which case null could have been passed in instead
 *     of a list).
 *
 * The directive also reads the angulardf-proxy-template attribute at creation
 * time. If it is set, it is used by to resolve URLs which would be forbidden
 * to fetch by CORS rules. A widespread value is
 * "http://data.fm/proxy?uri={uri}".
 *
 */
directive('angulardf', [function() {
    /* Monkey patching to rdflib happens here. */

    /* Explicitly defining a $$hashKey function keeps angular from assigning a
     * made-up $$hashKey to all objects.
     *
     * Note that this only reduces custom-id clobber. It does still not allow
     * ng-repeat over always freshly generated lists, because while the
     * ngRepeat directive itself uses $$hashKey, $watchCollectionInterceptor
     * compares for object identity.
     * */

    $rdf.Variable.prototype.$$hashKey = $rdf.Variable.prototype.toNT;;
    $rdf.Literal.prototype.$$hashKey = $rdf.Literal.prototype.toNT;;
    $rdf.NamedNode.prototype.$$hashKey = $rdf.NamedNode.prototype.toNT;;
    $rdf.BlankNode.prototype.$$hashKey = $rdf.BlankNode.prototype.toNT;;

    /* The actual angulardf directive */
    return {
        'restrict': 'A',
        'scope': true,
        'controller': ['$scope', '$attrs', function($scope, $attrs) {

            /** Not plainly exporting the $rdf.Namespace function as its
             * results don't have a stable identity, which makes them harder to
             * use with angular. */
            $scope.Namespace = function(nsuri) {
                var cache = {};
                return function (ln) {
                    if (!(ln in cache))
                        cache[ln] = new $rdf.NamedNode(nsuri + ln);
                    return cache[ln];
                };
            };

            $rdf.Fetcher.crossSiteProxyTemplate = $attrs.angulardfProxyTemplate;

            /** helper that does a statementsMatching, filters out a particular
             * attribute of the quadruplet, and ensures uniqueness */
            var _query = function(scope_or_ardf, what, s, p, o, w) {
                var result = [];
                var hashes = {};
                /* in case it is said from html where $rdf.sym is not available */
                if (typeof s == "string") {s = $rdf.sym(s); console.warn("Implicit string conversion for subject is deprecated:", s);}
                if (typeof p == "string") {p = $rdf.sym(p); console.warn("Implicit string conversion for predicate is deprecated:", p);}
                if (typeof o == "string") {o = $rdf.sym(o); console.warn("Implicit string conversion for object is deprecated:", o);}
                if (typeof w == "string") {w = $rdf.sym(w); console.warn("Implicit string conversion for why is deprecated:", w);}
                if (s === undefined || p === undefined || o === undefined || w === undefined) return [];

                if (s === null || s.length === undefined) s = [s];
                if (p === null || p.length === undefined) p = [p];
                if (o === null || o.length === undefined) o = [o];
                if (w === null || w.length === undefined) w = [w];

                for (var index_s = 0; index_s < s.length; ++index_s)
                    for (var index_p = 0; index_p < p.length; ++index_p)
                        for (var index_o = 0; index_o < o.length; ++index_o)
                            for (var index_w = 0; index_w < w.length; ++index_w) {
                                var matching = scope_or_ardf.ardf.graph.statementsMatching(s[index_s], p[index_p], o[index_o], w[index_w]);
                                for (var i in matching) {
                                    var hs = matching[i][what].hashString();
                                    if (hashes[hs] === undefined) {
                                        result.push(matching[i][what]);
                                        hashes[hs] = true;
                                    }
                                }
                            }
                return result;
            }

            $scope.subjects = function(p, o, w) {
                return _query(this, 'subject', null, arguments.length >= 1 ? p : null, arguments.length >= 2 ? o : null, arguments.length >= 3 ? w : null);
            }
            $scope.predicates = function(s, o, w) {
                return _query(this, 'predicate', arguments.length >= 1 ? s : null, null, arguments.length >= 2 ? o : null, arguments.length >= 3 ? w : null);
            }
            $scope.objects = function(s, p, w) {
                return _query(this, 'object', arguments.length >= 1 ? s : null, arguments.length >= 2 ? p : null, null, arguments.length >= 3 ? w : null);
            }
            $scope.whys = function(s, p, o) {
                return _query(this, 'why', arguments.length >= 1 ? s : null, arguments.length >= 2 ? p : null, arguments.length >= 3 ? o : null, null);
            }


            /* For easier handling of angulardf by other modules, all of the above
             * should sooner or later migrate into the ardf object instead of being
             * semi-bound to this scope. */

            var terms = {
                'type': $rdf.sym('http://www.w3.org/1999/02/22-rdf-syntax-ns#type'),
                'subClassOf': $rdf.sym('http://www.w3.org/2000/01/rdf-schema#subClassOf'),
            };

            var _graph = $rdf.graph();
            /* this is to keep firebug usable with an ardf object around.
             * firebug uses toString like a __repr__, and does not truncate it */
            _graph.toString = function(){return "An AngulaRDF context's graph";};
            $scope.ardf = {
                'graph': _graph,
                'fetch': $rdf.fetcher(_graph),
                'subjects': $scope.subjects,
                'predicates': $scope.predicates,
                'objects': $scope.objects,
                'whys': $scope.whys,

                /** Return true if a statement s p o exists. This is equivalent to
                 * whys(s, p, o).length != 0, but may be further optimized. */
                'exists': function exists(s, p, o) {
                    return this.whys(s, p, o).length != 0;
                },

                /** Short hand for exists(s, ns.rdf('type'), o) */
                'is_a': function is_a(s, t) {
                    return this.exists(s, terms.type, t);
                },

                /** A query for ?X where s p ?X and ?X a t. */
                'spt': function spt(s, p, t) {
                    var self = this;
                    return this.objects(s, p).filter(function(x) {
                        return self.is_a(x, t);
                    });
                },

                /** A query for ?X where ?X p o and ?X a t. */
                'po_typed_t': function po_typed_t(p, o, t) {
                    var self = this;
                    return this.subjects(p, o).filter(function(x) {
                        return self.is_a(x, t);
                    });
                },

                /** A query for ?X where s p ?X and ?X r ?Y. When using this, make
                 * sure you really want the ?X and not the ?Y (that would be
                 * objects(objects(s, p), r)).  */
                'spr': function spr(s, p, r) {
                    var self = this;
                    return this.objects(s, p).filter(function(x) {
                        return self.objects(x, r).length != 0;
                    });
                },

                /** Intersect two query result sets. Be prepared for this to be
                 * deprecated once a decent set implementation is used
                 * throughout angulardf. */
                'intersect': function intersect(first, second) {
                    if (first === undefined || second === undefined) {
                        return undefined;
                    }
                    var seen = {};
                    for (var i = 0; i < first.length; ++i) seen[first[i].hashString()] = true;
                    var result = [];
                    for (var i = 0; i < second.length; ++i) if (second[i].hashString() in seen) result.push(second[i]);
                    return result;
                },

                /** Form the difference between two query result sets (first -
                 * seconds), ie. the list of all elements in first but not in
                 * second. Be prepared for this to be deprecated once a decent
                 * set implementation is used throughout angulard. */
                'difference': function difference(first, second) {
                    var seen = {};
                    for (var i = 0; i < second.length; ++i) seen[second[i].hashString()] = true;
                    var result = [];
                    for (var i = 0; i < first.length; ++i) if (!(first[i].hashString() in seen)) result.push(first[i]);
                    return result;
                },

                /** Form the union of two query result sets (first + seconds),
                 * ie. the list of all elements in first or in second. Be
                 * prepared for this to be deprecated once a decent set
                 * implementation is used throughout angulard. */
                'union': function union(first, second) {
                    var seen = {};
                    var result = first.slice();
                    for (var i = 0; i < first.length; ++i) seen[first[i].hashString()] = true;
                    for (var i = 0; i < second.length; ++i) if (!(second[i].hashString() in seen)) result.push(second[i]);
                    return result;
                },

                /** Return a list that inludes the class(es) and all its/their known subclasses */
                'subclasses': function subclasses(base) {
                    if (!angular.isArray(base))
                        base = [base];
                    var out = base;
                    var lastlen = 0;
                    while (out.length != lastlen) {
                        lastlen = out.length
                        out = this.union(out, this.subjects(terms.subClassOf, out));
                    }
                    return out;
                },

                /** Return a list that inludes the class(es) and all its/their known superclasses */
                'superclasses': function superclasses(base) {
                    if (!angular.isArray(base))
                        base = [base];
                    var out = base;
                    var lastlen = 0;
                    while (out.length != lastlen) {
                        lastlen = out.length
                        out = this.union(out, this.objects(out, terms.subClassOf));
                    }
                    return out;
                },

                /** Return a copy of the given list of nodes, sorted by the
                 * literal values of the entries' given property.
                 *
                 * So far, this only compares numbers expressed in a literal
                 * property. */
                'sorted': function sorted(array, property) {
                    var self = this;
                    var decorated = array.map(function getval(n) {
                        var os = self.objects(n, property);
                        if (os.length != 1 || os[0].termType != 'Literal')
                            return {'v': n, 's': Infinity};
                        else
                            return {'v': n, 's': parseFloat(os[0].value) || Infinity};
                    });
                    decorated.sort(function (a, b) { return a.s - b.s; });
                    return decorated.map(function (d) { return d.v; });
                }
            };

            /** This allows the _query style functions to be called as methods of scope as well as as methods of ardf. */
            $scope.ardf.ardf = $scope.ardf;

            /** This makes angular.js refresh when changes arrive from rdflib. */
            $scope.__dirty = false;
            var refresh = function(reason) {
                return function() {
                    $scope.__dirty = true;
                    setTimeout(function() {
                        if ($scope.__dirty) $scope.$apply();
                        $scope.__dirty = false;
                    });
                    $scope.ardf.fetch.addCallback(reason, refresh(reason));
                };
            };
            $scope.ardf.fetch.addCallback('done', refresh('done'));
            $scope.ardf.fetch.addCallback('refresh', refresh('refresh'));
            $scope.ardf.fetch.addCallback('fail', refresh('fail'));
        }]
    };
}]).

/** The angulardf-preload="<URL>" attribute can be used to fetch a specified
 * resource (relative to the loaded document) immediately.
 *
 * Only use inside or at the same element as an angulardf directive. */
directive('angulardfPreload', [
    '$location',
    function($location) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                scope.$watch(function() {
                        return attrs.angulardfPreload;
                    },
                    function(value) {
                        /** @FIXME use a better rterm */
                        var values = value.split(/[ \n\t]+/);
                        for (var i = 0; i < values.length; ++i)
                            scope.ardf.fetch.requestURI($rdf.uri.join(values[i], $location.absUrl()), $rdf.sym($location.absUrl()));
                    }
                );
            }
        };
    }
]).

/** The angulardf-preload-with-credentials="<URL>" attribute is a variant of
 * angulardf-preload that will set the options for authenticated
 * XMLHttpRequests. */
directive('angulardfPreloadWithCredentials', [
    '$location',
    function($location) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                scope.$watch(function() {
                        return attrs.angulardfPreloadWithCredentials;
                    },
                    function(value) {
                        /** @FIXME use a better rterm */
                        var values = value.split(/[ \n\t]+/);
                        for (var i = 0; i < values.length; ++i)
                            scope.ardf.fetch.requestURI($rdf.uri.join(values[i], $location.absUrl()), $rdf.sym($location.absUrl()), {'withCredentials': true});
                    }
                );
            }
        };
    }
]).

/** Provide a function that allows directives with isolated scopes to
 * explicitly un-isolate the angulardf variables.
 *
 * In a directive that is generally isolated but wants to use the angulardf
 * infrastructure provided by the parent (as if it was a service, but still
 * allowing scoped instanciation), use the 'angulardfIsolatedScopeHack' service
 * in the controller and call
 *
 *     angulardfIsolatedScopeHack($scope);
 *
 * inside it.
 *
 * As the name implies, this is a hack. Envisioned alternatives include:
 *
 * * using angulardf as a service -- but that would just completely defy
 *   isolated scopes (instead of opting in), and wouldn't allow running two
 *   angulardf instances in parallel on distinct dom nodes.
 *
 * * explicitly passing angulardf down via attributes -- that would require all
 *   scope provided objects to be children of one object that can be explicitly
 *   named (a nice idea, think of an explicit variable my_angulardf instead of
 *   global-per-scope), but is too verbose for angulardf style, and besides, it
 *   requires the user of the isolating directive to explicitly hand down the
 *   object instead of the isolating directive grabbing it; still a possibility
 *   if implemented correctly.
 *
 * */
factory('angulardfIsolatedScopeHack', [function() {
    return function($scope) {
        $scope.ardf = $scope.$parent.ardf;

        $scope.subjects = $scope.$parent.subjects;
        $scope.predicates = $scope.$parent.predicates;
        $scope.objects = $scope.$parent.objects;
        $scope.whys = $scope.$parent.whys;
        $scope.ns = $scope.$parent.ns;
    }
}])

;
