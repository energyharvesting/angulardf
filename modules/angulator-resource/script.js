"use strict";
// vim:et:sw=4:ts=4:sts=4

var resourcetemplate = '\
<div class="angulator-resource" ng-class="{expand: expand}" ng-if="topic.termType !== &quot;Literal&quot;">\
    <div ng-class="instance.gotoclasses(topic)">\
    <input type="checkbox" ng-model="expand" /> \
    <angulator-document></angulator-document>\
      <span class="resource-label" title="{{ topic.termType == &quot;NamedNode&quot; ? topic.uri : topic.termType }}" ng-click="instance.prepareGoto(topic)">{{ instance.label(topic) }}</span> \
      <a ng-click="instance.refocus(parentToplevel, topic)" ng-if="parentToplevel !== topic">↰</a><a ng-click="instance.drop(topic)" ng-if="parentToplevel === topic">×</a>\
      <a ng-if="topic.uri.substr(0, 4) == \'http\'" ng-href="{{ topic.uri }}" target="_blank" alt="Open in new browser tab">→</a>\
      </div>\
    <table ng-if="expand">\
        <tr ng-repeat="p in instance.ardf.predicates(topic)"><td ng-class="instance.gotoclasses(p)" ng-click="instance.prepareGoto(p)">{{ instance.label(p) }}</td><td>\
                <div ng-repeat="o in instance.ardf.objects(topic, p)"><angulator-resource object="o" /></div>\
            </td>\
        </tr>\
        <tr ng-repeat="p in (instance.includeInverse ? instance.ardf.predicates(null, topic) : [])"><td ng-class="instance.gotoclasses(p)" ng-click="instance.prepareGoto(p)"><i>is</i> {{ instance.label(p) }} <i>of</i></td><td>\
                <div ng-repeat="s in instance.ardf.subjects(p, topic)"><angulator-resource object="s" /></div>\
            </td>\
        </tr>\
        <tr ng-if="instance.includeWhyData && instance.ardf.subjects(null, null, topic).length != 0"><td><i>contains statements about</i></td><td>\
                <div ng-repeat="s in instance.ardf.subjects(null, null, topic)"><angulator-resource object="s" /></div>\
            </td>\
        </tr>\
    </table>\
</div>\
<div class="angulator-resource literal" ng-if="topic.termType === &quot;Literal&quot;">{{ topic.value }}</div>\
';

/* who says angularjs directives can't do recursion? */
for (var j = 5; j >= 0; --j) {
    (function(i) {


        angular.module('angulator').directive('angulatorResource' + Array(i + 1).join('Sub'), function() {
            return {
                restrict: 'E',
                template: resourcetemplate.replace(/<angulator-resource/g, '<angulator-resource' + Array(i + 2).join('-sub')),
                scope: true,
                link: function($scope, iElement, iAttrs) {
                    $scope.expand = false;
                    if ('object' in iAttrs) {
                        $scope.topic = undefined;
                        $scope.$watch(iAttrs.object, function watchAction(value) {
                            $scope.topic = value;
                        });
                    } else {
                        $scope.topic = Resource(iAttrs['url'], $scope.store);
                    }
                }
            };
        });

    })(j);
}

angular.module('angulator').directive('angulatorDocument', function() {
    return {
        restrict: 'E',
        template: '<a title="{{ instance.resource_document(topic).toString() }}" class="resourcedoc {{ instance.ardf.fetch.getState(instance.resource_document(topic).uri) }}" ng-click="instance.ardf.fetch.requestURI(instance.resource_document(topic), undefined, {withCredentials: $event.shiftKey, force: $event.ctrlKey})" ng-if="topic.uri.substr(0, 4) === \'http\'">●</a>'
    }
});
