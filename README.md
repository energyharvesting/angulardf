AngulaRDF -- Using RDF in angular.js
====================================

The AngulaRDF module is a wrapper around the [rdflib.js][] [RDF][] library, tuned
to be usable with ease in [angular.js][].

To use it, load the `modules/angulardf/script.js` code into your Angular
application, and make your module/application depend on the module named
`angulardf`. It provides an `angulardf` directive that provides an `ardf`
object, and should be included in the outmost element that accesses RDF data.

Angulator
=========

The Angulator is a demo application built with AngulaRDF, which can be used to
explore RDF data sets. It tries to mimick the [tabulator][] in look and feel, but
is much more of a simple demo than a tabulator replacement. It can be used both
as a reference of how AngulaRDF can be used, and as a debugging tool inside
applications.

To get be able to run the Angulator in the project git repository, run [make][]
in the `vendor/` directory; this will download the relevant third party
libraries.

You can also run the [online version][].

State of the project
====================

AngulaRDF and Angulator are useable components, but some of the angular.js
mechanisms used are still subject to change. (For example, angulator has been
ported to "controller as" style, while AngulaRDF does something similar
manually by providing a directive which creates an object on the scope which is
unconditionally called `ardf`, and there are legacy variables around outside of
that on the scope.

The project has its canonical project page [at GitLab][].

License
=======

For the time being, the AngulaRDF project is licensed under the terms of the
[AGPL 3][] or later. We kindly ask prospective contributors to publish their
patches under the terms of the [MIT license][] until we've made up our minds
which license in that spectrum is going to be used on the long run.


[rdflib.js]: http://github.com/linkeddata/rdflib.js/
[RDF]: http://en.wikipedia.org/wiki/Resource_Description_Framework
[angular.js]: http://angularjs.org/
[tabulator]: http://www.w3.org/2005/ajar/tab
[AGPL 3]: https://www.gnu.org/licenses/agpl-3.0.html
[MIT license]: http://opensource.org/licenses/MIT
[make]: https://en.wikipedia.org/wiki/Make_(software)
[online version]: https://energyharvesting.gitlab.io/angulardf/
[at GitLab]: https://gitlab.com/energyharvesting/angulardf/
