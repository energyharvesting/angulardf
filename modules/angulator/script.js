"use strict";
// vim:et:sw=4:ts=4:sts=4

angular.module('angulator', ['angulardf'])

.controller('angulator', ['$scope', '$attrs', function($scope, $attrs) {
    this.ardf = $scope.ardf;

    this.urlToGoTo = $attrs.urlToGoTo;
    this.toplevels = [];
    this.includeInverse = false;
    this.showRaw = false;

    this.newToplevel = function(node) {
        if (node.termType === 'NamedNode')
            this.urlToGoTo = node.uri;
        for (var i = 0; i < this.toplevels.length; ++i)
            if (this.toplevels[i].$$hashKey() == node.$$hashKey()) return;
        this.toplevels.push(node);
    }
    this.newToplevelSym = function(url) {
        this.newToplevel($rdf.sym(url));
    }

    this.refocus = function(parentToplevel, topic) {
        this.toplevels[this.toplevels.indexOf(parentToplevel)] = topic;
    };

    this.drop = function(topic) {
        this.toplevels.splice(this.toplevels.indexOf(topic), 1);
    };
    this.prepareGoto = function(resource) {
        this.urlToGoTo = resource.uri;
    }
    this.gotoclasses = function(resource) {
        if (resource === undefined) return {}; /* how does this happen? */
        if (this.urlToGoTo == resource.uri)
            return {"current-goto": true};
        return {};
    }

    this.resource_document = function(node) {
        if (node === undefined) return undefined;
        if (node.termType !== 'NamedNode') return undefined;
        if (node.uri.indexOf('#') === -1) return node;
        return $rdf.sym(node.uri.substr(0, node.uri.indexOf('#')));
    };

    var _fallback_label = function(node) {
        if (node.termType !== 'NamedNode')
            return node.toNT()
        var slashsplit = node.uri.split('/');
        var hashsplit = slashsplit[slashsplit.length - 1].split('#');
        var lastpart = hashsplit[hashsplit.length - 1];
        if (lastpart) return lastpart;
        if (hashsplit.length > 2) {
            lastpart = hashsplit[hashsplit.length - 2];
            if (lastpart) return lastpart;
        }
        return node.uri;
    }
    this.label = function(node) {
        if (node.termType === 'literal') return node.value;

        var rdflabel = $rdf.sym("http://www.w3.org/2000/01/rdf-schema#label");

        var candidates = this.ardf.graph.each(node, rdflabel);

        if (candidates.length == 0)
            return _fallback_label(node);

        /* prefer english or language-independent terms */
        candidates.sort(function(b, a) { return ((a.lang === "en") * 2 + (a.lang === undefined)) - ((b.lang === "en") * 2 + (b.lang === undefined))});

        return candidates[0].toString();
    };
}])

.directive('angulator', [function() {
    return {
        'restrict': 'E',
        'templateUrl': 'modules/angulator/main.html',
        'scope': {'instance': '='},
    };
}]);
